package chat.client;

import chat.network.TCPConnection;
import chat.network.TCPConnectionListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

public class ClientWindow extends JFrame implements ActionListener, TCPConnectionListener { //расширились через extends фреймферком swing, для графического интерефейса
    //реализует интерфейс ActionListener, Alt + Enter

    private static final String IP_ADDR = "192.168.0.194";
    private static final int PORT = 8189;
    private static final int WIDTH = 600;
    private static final int HIEGHT = 400;

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new ClientWindow(); //чтобы выполнился в потоке edt
            }
        });

    }

    private final JTextArea log = new JTextArea(); //поле куда будем писать текст
    private final JTextField fieldNickname = new JTextField("alex"); //однострочное поле
    private final JTextField fieldInput = new JTextField();
    //графичиские интерфейсы как провило работают только с одном(главным) потоком

    private TCPConnection connection;

    private ClientWindow() {
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE); //операция закрытия крестиком
        setSize(WIDTH, HIEGHT); //размер окна
        setLocationRelativeTo(null); //всегда по центру
        setAlwaysOnTop(true); //всегда окно появяется сверху
        log.setEditable(false); //запрещено редактирование
        log.setLineWrap(true); //перенос слов
        add(log, BorderLayout.CENTER); //BorderLayout - команда отвечающая за расположение на экране

        fieldInput.addActionListener(this); //добавили себя, если этого не сделать, не будет ловить нажатие на Enter
        add(fieldInput, BorderLayout.SOUTH);
        add(fieldNickname, BorderLayout.NORTH);

        setVisible(true); //окно будет видимым
        try {
            connection = new TCPConnection(this, IP_ADDR, PORT); //СЛУШАЕТ tcp соединение
        } catch (IOException e) {
            printMsg("Connection exception: " + e); //метод используется из потока окошка
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String msg = fieldInput.getText();
        if(msg.equals("")) return;
        fieldInput.setText(null);
        connection.sendString(fieldNickname.getText() + ": " + msg);
    }


    @Override
    public void onConnectionReady(TCPConnection tcpConnection) {
        printMsg("Connection ready..."); //метод используется из потока соединения
    }

    @Override
    public void onReceiveString(TCPConnection tcpConnection, String value) {
        printMsg(value);
    }

    @Override
    public void onDisconnect(TCPConnection tcpConnection) {
        printMsg("Connection close");
    }

    @Override
    public void onException(TCPConnection tcpConnection, Exception e) {
        printMsg("Connection exception: " + e);
    }

    //будем с ним работать с разных поток, поток с окошка и поток соединения поэтому synchronized
    private synchronized void printMsg(String msg) {
        //нельзя log.add(), т.к будет вызываться из разных потоков,поэтому надо полностью..
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                log.append(msg + "\n");
                log.setCaretPosition(log.getDocument().getLength()); //чтобы текст автоматически всегда поднимал в место autoscroll
                //гарантированно будет выполнено в потоке окна
            }
        });
    }
}
