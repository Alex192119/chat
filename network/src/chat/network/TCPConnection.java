package chat.network;

import java.io.*;
import java.net.Socket;
import java.nio.charset.Charset;

public class TCPConnection {

    private final Socket socket;                        //сокет
    private final Thread rxThread;                      //поток(событийная система)
    private final TCPConnectionListener eventListener;  //eventLIstener - это интерфейс
    private final BufferedReader in;
    private final BufferedWriter out;

    //конструктор если кто то снаружи уже создаст сокет, рассчитан что сокет будет создаваться непосредственно внутри
    //это написан конструктор, в его скобках сигнатура
    //этот конструктор создает сокет
    public TCPConnection(TCPConnectionListener eventListener, String ipAddr, int port) throws  IOException {
        this(eventListener, new Socket(ipAddr, port)); //вызвали из этого конструктора, другой(eventListener
    }

    //конструктор для соединения, уже берет готовый сокет
    public TCPConnection(TCPConnectionListener eventListener, Socket socket) throws IOException {
        this.eventListener = eventListener;
        this.socket = socket;   //remember the socket
        in = new BufferedReader(new InputStreamReader(socket.getInputStream(), Charset.forName("UTF-8")));
        /*Смотреть из внутренних скобок,
        socket.getInputStream()                         - простой поток ввода
        new InputStreamReader(socket.getInputStream())  - создали, он может читать что то сложнее чем байты
        new BufferedReader                              - создали экземпляр, читать и писать строчки
         */
        out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream(), Charset.forName("UTF-8")));
        // аналогично
        // create new Thread
        rxThread = new Thread(new Runnable() {          // анонимно создали класс
            @Override
            public void run() { // hear input connextion
                try {
                    eventListener.onConnectionReady(TCPConnection.this); //объект соединения
                    while(!rxThread.isInterrupted()) {   //бескон. цикл, пока поток не прерван
                        String msg = in.readLine();     //получаем строчку
                        eventListener.onReceiveString(TCPConnection.this, msg); //передаем объект соединения и саму одну строчку
                    }

                } catch (IOException e) {
                    eventListener.onException(TCPConnection.this, e);
                } finally { //даже если ошибка надо закрыть сокет
                    eventListener.onDisconnect(TCPConnection.this);
                }
            }
        });    //Слушает, ждет поток
        rxThread.start();

    }
    //Пишем метод отправить сообщение и метод преравать соединение
    public synchronized void sendString(String value) {
        try {
            out.write(value + "\r\n"); //нет символа конца строки, от себя возврат коретки и перевод строки
            out.flush();
        } catch (IOException e) {
            eventListener.onException(TCPConnection.this, e);
            disconnect(); //схватили исключение, отправили исключение, остановили поток
        }

    }

    public synchronized void disconnect() {
        rxThread.interrupt();
        try {
            socket.close();
        } catch (IOException e) {
            eventListener.onException(TCPConnection.this, e);
        }
    }

    //наследовали у предка, сделали свою реализацию -> реализовали полиморфизм
    @Override
    public String toString() {
        return "TCPConnection: " + socket.getInetAddress() + ": " + socket.getPort(); //так будут выглядеть сообщения в логах
    }
}
