package chat.network;

public interface TCPConnectionListener {

    void onConnectionReady(TCPConnection tcpConnection); //готовое соединение, () - даем доступ
    void onReceiveString(TCPConnection tcpConnection, String value); //get String
    void onDisconnect(TCPConnection tcpConnection);
    void onException(TCPConnection tcpConnection, Exception e);
}
